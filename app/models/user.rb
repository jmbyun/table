class User < ActiveRecord::Base
	def get_payback_infos
		result = []
		balance = self.balance
		users = User.order('balance ASC')
		while balance < 0
			user = users.pop
			if -balance < user.balance
				result.push({:user => user, :amount => -balance})
				break
			else
				result.push({:user => user, :amount => user.balance})
				balance += user.balance
			end
		end
		return result
	end
	def get_retrieval_infos
		result = []
		balance = self.balance
		users = User.order('balance DESC')
		while balance > 0
			user = users.pop
			if balance < -user.balance
				result.push({:user => user, :amount => balance})
				break
			else
				result.push({:user => user, :amount => -user.balance})
				balance += user.balance
			end
		end
		return result
	end
	def bar_percent
		users = User.all
		balances = users.map{|user| user.balance}
		max_abs_balance = [-balances.min, balances.max].max
		if self.balance > 0
			return self.balance.to_f * 100 / max_abs_balance
		else
			return -(-self.balance.to_f * 100 / max_abs_balance)
		end
	end
	def self.sum_of_balances
		users = User.all
		balances = users.map{|user| user.balance}
		return balances.sum
	end
end
