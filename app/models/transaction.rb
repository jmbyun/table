class Transaction < ActiveRecord::Base
	belongs_to :from_user, :class_name => 'User'
	belongs_to :to_user, :class_name => 'User'
	after_create :reflect_to_accounts
	before_destroy :reflect_to_accounts_destroy

	private
		def reflect_to_accounts
			result = self.from_user.increment!(:balance, self.amount)
			result = result and self.to_user.decrement!(:balance, self.amount)
			unless result
				raise 'Failed to reflect transactions to accounts.'
			end
		end

		def reflect_to_accounts_destroy
			result = self.from_user.decrement!(:balance, self.amount)
			result = result and self.to_user.increment!(:balance, self.amount)
			unless result
				raise 'Failed to reflect transaction cancellation to accounts.'
			end
		end
end
