class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	helper_method :authorize

	protected
	  	def authorize
	  		if authenticated?
	  			return true
	  		else
	  			redirect_to '/auth'
	  		end
	  	end

	private
		def authenticated?
			if session[:auth] and session[:auth] == true
				@authenticated = true
			else
				@authenticated = false
			end
		end
end
