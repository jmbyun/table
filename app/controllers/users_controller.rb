class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :authorize, only: [:index, :edit, :new, :create, :update, :destroy, :event_new, :event_create]
  skip_before_filter :verify_authenticity_token, :only => [:event_create, :auth]

  def login

  end

  def logout
    session[:auth] = false
    redirect_to root_url
  end

  def auth
    # require 'BCrypt'
    salt = '$2a$10$kERdpMU2H/Lu9hJmYkaZv.'
    hash = '$2a$10$kERdpMU2H/Lu9hJmYkaZv.hb3bXMrObF1nuUCull4Ah9WVc8hg9mO'
    if BCrypt::Engine.hash_secret(params[:passcode], salt) == hash
      session[:auth] = true
      redirect_to root_url
    else
      flash[:msg] = 'Invalid passcode.'
      redirect_to '/auth'
    end
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.order(:name)
  end

  # GET /users/1
  # GET /users/1.json
  def show
    if @user.balance < 0
      @payback_infos = @user.get_payback_infos
    elsif @user.balance > 0
      @retrieval_infos = @user.get_retrieval_infos
    end

  end

  def event_new
    @msg = params[:msg].to_s
    @negative = params[:negative].to_s
    @multi_target = params[:multi_target].to_s == 'true'
    @desc = params[:desc].to_s == 'true'
    @user = User.find(params[:user_id])
    @users = User.all
  end

  def event_create
    amount = params[:amount].to_i
    negative = params[:negative].to_s
    user_id = params[:user_id].to_s
    target_ids = params[:target_id].to_s.split(',')
    desc = params[:desc].to_s
    if negative == 'true'
      amount = -amount
    end
    if target_ids.length > 1
      amount = amount / target_ids.length
    end
    result = true
    begin
      target_ids.each do |target_id|
        target_id = target_id.strip
        if user_id.to_i != target_id.to_i
          transaction = Transaction.new({
            :from_user_id => user_id.to_i,
            :to_user_id => target_id.to_i,
            :amount => amount,
            :description => desc
          })
          result = result and transaction.save
        end
      end
    rescue
      user = User.find(user_id)
      redirect_to user, notice: 'Transaction went wrong.'
      return
    end
    if result
      user = User.find(user_id)
      redirect_to user, notice: 'Transaction was successfully processed.'
      return
    else
      user = User.find(user_id)
      redirect_to user, notice: 'Transaction went wrong.'
      return
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  # def edit
  # end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  # def destroy
  #   @user.destroy
  #   respond_to do |format|
  #     format.html { redirect_to users_url }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :account_info, :balance, :is_removed)
    end
end
