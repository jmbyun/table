json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :from_user_id, :to_user_id, :amount, :description
  json.url transaction_url(transaction, format: :json)
end
