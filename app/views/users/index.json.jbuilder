json.array!(@users) do |user|
  json.extract! user, :id, :name, :account_info, :balance
  json.url user_url(user, format: :json)
end
