class AddIsRemovedToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :is_removed, :boolean, default: false
  end
end
