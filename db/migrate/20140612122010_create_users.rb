class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :account_info
      t.integer :balance

      t.timestamps
    end
  end
end
